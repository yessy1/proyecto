<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ListaArticulosData{
    function listarArticulos(){
        $articulos= [];
	   $con = new Conexion();
        $conexion= $con->getConnection();
        $resultado = $conexion->query("SELECT * FROM articulo");
        $resultado->data_seek(0);
       
        while ($fila = $resultado->fetch_assoc()) {
            $prod = new Articulo();
            $prod->setCodigo($fila ['codigo']);
            $prod->setNombre($fila ['nombre']);
            $prod->setDescripcion($fila ['descripcion']);
            $prod->setStock($fila ['stock']);
            $prod->setPrecioCompra($fila ['precioCompra']);
            $prod->setPrecioVenta($fila ['precioVenta']) ;
            $prod->setGanancia($fila ['ganancia']);
            
            array_push($articulos, $prod);
            
        }
        if ($resultado->num_rows>0) {
		
		return $articulos ; 
		}
    }
}
