<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articulo
 *
 * @author Israel
 */
class Articulo {
    public $id;
    public $codigo;
    public $nombre;
    public $descripcion;
    public $stock;
    public $precioCompra;
    public $precioVenta;
    public $ganancia;
    
    function __construct() {
        
    }

    
    function getId() {
        return $this->id;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getStock() {
        return $this->stock;
    }

    function getPrecioCompra() {
        return $this->precioCompra;
    }

    function getPrecioVenta() {
        return $this->precioVenta;
    }

    function getGanancia() {
        return $this->ganancia;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setPrecioCompra($precioCompra) {
        $this->precioCompra = $precioCompra;
    }

    function setPrecioVenta($precioVenta) {
        $this->precioVenta = $precioVenta;
    }

    function setGanancia($ganancia) {
        $this->ganancia = $ganancia;
    }


}
