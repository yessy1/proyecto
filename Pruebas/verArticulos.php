<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/OperacionesTablas.js"></script>
        <meta charset="UTF-8">
        <title>Lista de artículos</title>
        
        <p role="separator" class="divider"></p>
        <?php
            include ("masterPage.php");
            require '../ProyectoPHPAnalisis2017/Data/ListaArticulosData.php';
        ?>
    </head>
    <body class="container">   
        
        <div class="jumbotron" >
            <table class="table" id="tablaArt">
                <thead>
                    <tr >
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Stock</th>
                        <th>Compra</th>
                        <th>Venta</th>
                        <th>Ganancia</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $articulos= listarArticulos();
                        foreach($articulos as $valor)
                        {    
                            echo "<tr id=".$valor->codigo.">\n ";  
                            echo "<td >" . $valor->codigo . "</td>\n";
                            echo "<td contenteditable>" . $valor->nombre . "</td>\n";
                            echo "<td contenteditable>" . $valor->descripcion . "</td>\n";
                            echo "<td contenteditable>" . $valor->stock . "</td>\n";
                            echo "<td >" . $valor->precioCompra . "</td>\n";
                            echo "<td contenteditable>" . $valor->precioVenta . "</td>\n";
                            echo "<td contenteditable>" . $valor->ganancia . "</td>\n";
                            echo "<td><button class=".'btn-danger'." onClick=".'borrarFila("'.$valor->codigo.'")'."  >Eliminar</button></td>\n";
                            echo "<td>><button class=".'btn-warning'." onclick=".'modificarFila("'.$valor->codigo.'")'." >Modificar</button></td>\n";
                            echo "</tr>\n";
                              
                        }
                        
                    ?>
                    
                </tbody >
            </table>
        </div>
        
       <!-- <script type="text/javascript" src="js/funcionesAjax.js"></script>-->
    </body>
</html>
